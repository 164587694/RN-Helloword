import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    SectionList,
} from 'react-native';

export default class SecondPage extends Component {

    static navigationOptions = {
        title: 'SecondPageTitle',
    };

    //构建斐波那契数据
    getFibonacciArray(max) {
        var fibonacci = [];
        fibonacci[0] = {key: "0", value: 1};
        fibonacci[1] = {key: "1", value: 1};
        for (var i = 2; i < max; i++) {
            fibonacci[i] = {key: '' + i, value: fibonacci[i - 2].value + fibonacci[i - 1].value}
        }
        console.log(fibonacci);

        return fibonacci;
    }

    getDemoJsonArray() {
        var array = [
            {key: "1", value: "1"},
            {key: "2", value: "1"},
            {key: "3", value: "2"},
            {key: "4", value: "3"},
            {key: "5", value: "5"},
            {key: "6", value: "8"},
            {key: "7", value: "13"},
            {key: "8", value: "21"},
            {key: "9", value: "34"},
            {key: "10", value: "55"},
            {key: "11", value: "89"},
            {key: "12", value: "144"},
            {key: "13", value: "233"},
            {key: "14", value: "377"},
            {key: "15", value: "610"},
        ]
        array.reverse();
        return array;
    }

    _renderItem = (({item}) =>
            <Text style={styles.welcome}>{item.value}</Text>
    );

    render() {
        const {navigation} = this.props;
        const isFibonacci = navigation.getParam('isFibonacci', 'N');
        if (isFibonacci === 'Y') {
            return (
                <View style={{flex: 1}}>
                    <SecondPageHeader/>
                    <Text style={styles.welcome}>This is Fibonacci</Text>
                    <FlatList
                        data={this.getDemoJsonArray()}
                        renderItem={this._renderItem}
                    />
                    <FlatList
                        data={this.getFibonacciArray(15)}
                        renderItem={this._renderItem}
                    />
                </View>
            )
        } else {
            return (
                <View style={{flex: 1}}>
                    <SecondPageHeader/>
                    <Text style={styles.welcome}>no Fibonacci</Text>
                    <SectionList
                        renderItem={({item, index, section}) => <Text key={index} style={styles.welcome}>{item}</Text>}
                        renderSectionHeader={({section: {title}}) => (
                            <Text style={{
                                color: '#e00',
                                fontSize: 10,
                                fontWeight: "bold",
                                textAlign: 'center'
                            }}>{title}</Text>
                        )}
                        sections={[
                            {title: "Title1", data: ["item1", "item2"]},
                            {title: "Title2", data: ["item3", "item4", "item4", "item4", "item4"]},
                            {title: "Title3", data: ["item5", "item6"]},
                            {title: "Title3", data: ["item5", "item6"]}
                        ]}
                        keyExtractor={(item, index) => item + index}
                    />
                </View>
            )
        }
    }
}

class SecondPageHeader extends Component {
    render() {
        return (
            <View>
                <Text style={styles.welcome}>This is SecondPage</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        color: '#000',
        textAlign: 'center',
        margin: 10,
    },
});
