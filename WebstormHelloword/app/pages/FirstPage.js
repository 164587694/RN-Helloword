import React, {Component} from 'react';
import {StyleSheet, Alert, Text, View, Button} from 'react-native';

export default class FirstPage extends React.Component {

    //标题栏
    static navigationOptions = {
        title: 'FirstPageTitle',
    };

    _onPressButton(props) {
        Alert.alert('跳转页面',
            '点击确定跳转页面',
            [
                {text: '不了'},
                {
                    text: '普遍列表123',
                    onPress: () => {
                        props.navigation.navigate('SecondConst', {isFibonacci: "N"});
                    }
                },
                {
                    text: '斐波那契数列',
                    onPress: () => {
                        props.navigation.navigate('SecondConst', {isFibonacci: "Y"});
                    }
                },
            ],
            {cancelable: false});
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>This is FirstPage</Text>
                <Button
                    onPress={() => this._onPressButton(this.props)}
                    title="press"
                    color="#cdadac"
                    accessibilityLabel="this this a purple button"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        color: '#000',
        textAlign: 'center',
        margin: 10,
    },
});
