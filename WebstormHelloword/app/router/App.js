/**
 * 应用路由及主容器
 *
 * react-navigation
 *
 */
import React, {Component} from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import FirstPage from "../pages/FirstPage";
import SecondPage from "../pages/SecondPage";

const MainNavigator = createStackNavigator({
    //路由配置
    FirstConst: {
        screen: FirstPage
    },
    SecondConst: {
        screen: SecondPage
    },
}, {
    //主页面
    initialRouteName: "FirstConst"
});
const AppContainer = createAppContainer(MainNavigator);

export default class App extends React.Component {
    render() {
        return <AppContainer/>
    }
}