/**
 * 使用flux路由
 */

import React, {Component} from 'react';
import {RouterClass} from './router';

export default class App extends Component {

    render() {
        return <RouterClass/>
    }
}