import React, {Component, PropTypes} from 'react';
import {
    Router,
    Scene,
    Reducer,
} from 'react-native-router-flux';
import FirstFluxPage from "../pages/FirstFluxPage";
import SecondFluxPage from "../pages/SecondFluxPage";
import {Page} from "../constants";

const reducerCreate = params => {
    const defaultReducer = Reducer(params);
    return (state, action) => {
        return defaultReducer(state, action);
    }
};

export const RouterClass = () => (
    <Router createReducer={reducerCreate} sceneStyle={{backgroundColor: '#fef'}}>
        <Scene key="root">
            <Scene
                key={Page.FIRST_PAGE}
                title="第一个页面"
                component={FirstFluxPage}
                initial={true}
            />
            <Scene
                key={Page.SECOND_PAGE}
                title="第二个页面"
                component={SecondFluxPage}
            />
        </Scene>
    </Router>
);