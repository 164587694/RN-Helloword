/**
 * 路由地址枚举
 * @type {string}
 */
export const FIRST_PAGE = "first_page";
export const SECOND_PAGE = "second_page";