/**
 * app配置
 */
import {AppRegistry} from 'react-native';
// import App from './app/router/App';
import App from './app/fluxrouter/FluxApp';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
